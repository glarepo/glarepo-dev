# パッケージルートに入る
cd package

# .SRCINFOとPKGBUILD.sha512sumを更新する
for dir in (cat ../updated_package_list)
  cd $dir
  sha512sum PKGBUILD > PKGBUILD.sha512sum
  su builder -c 'makepkg --printsrcinfo > .SRCINFO'
  cd ../
end
