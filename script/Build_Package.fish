cd package

for dir in (cat ../updated_package_list)
  cd $dir
  su builder -c 'makepkg -s --noconfirm'
  cd ../
end
