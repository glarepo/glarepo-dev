# パッケージルートに入る
cd package

# パッケージの更新があったか
set is_updated false

# 再帰的にパッケージのディレクトリに入りハッシュ値の確認をする
for dir in (ls -d */)
  cd $dir
  if [ ! -e ./PKGBUILD ]
    echo "PKGBUIDが存在しません"
    cd ../
    continue
  else if [ ! -e ./PKGBUILD.sha512sum ]
    set is_updated true
    cd ../
    echo $dir >> ../updated_package_list
    continue
  else if [ -n "sha512sum -c PKGBUILD.sha512sum | grep -o FAILED" ]
    # ハッシュ値が一致した場合次のパッケージの確認へ
    cd ../
    continue
  else
    set is_updated true
    cd ../
    echo $dir >> ../updated_package_list
    continue
  end
end
